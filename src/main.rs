mod args;

use clap::Parser;
use std::collections::HashSet;
use std::env;

fn main() {
    let args::CmdArgs {
        append,
        prepend,
        path,
        unique,
    } = args::CmdArgs::parse();

    match (path, unique) {
        (Some(path), true) => println!(
            "{}",
            from_path_unique(prepend, split_path(&path), append).join(":")
        ),
        (Some(path), false) => println!(
            "{}",
            concatenate(prepend, split_path(&path), append).join(":")
        ),
        (None, true) => println!("{}", from_env_unique(append, prepend).join(":")),
        (None, false) => println!("{}", from_env(append, prepend).join(":")),
    }
}

fn from_env_unique(append: Vec<String>, prepend: Vec<String>) -> Vec<String> {
    from_path_unique(
        prepend,
        split_path(&env::var("PATH").unwrap()).into_iter().collect(),
        append,
    )
}

fn from_path_unique(prepend: Vec<String>, path: Vec<String>, append: Vec<String>) -> Vec<String> {
    let path = unique(path);
    let appended = append_unique(append, &path);
    concatenate(prepend_unique(prepend, &path), path, appended)
}

fn from_env(append: Vec<String>, prepend: Vec<String>) -> Vec<String> {
    let path: Vec<String> = split_path(&env::var("PATH").unwrap()).into_iter().collect();
    concatenate(prepend, path, append)
}

fn concatenate(prepend: Vec<String>, path: Vec<String>, append: Vec<String>) -> Vec<String> {
    let mut collected = prepend;
    collected.extend(path);
    collected.extend(append);
    collected
}

fn prepend_unique(prepend: Vec<String>, path: &[String]) -> Vec<String> {
    let pth: HashSet<&String> = path.iter().collect();
    let acc = Vec::with_capacity(prepend.len());
    prepend
        .into_iter()
        .rev()
        .fold(acc, |mut acc, p| {
            if !pth.contains(&p) && !acc.contains(&p) {
                acc.push(p);
            }
            acc
        })
        .into_iter()
        .rev()
        .collect()
}

fn append_unique(append: Vec<String>, path: &[String]) -> Vec<String> {
    let pth: HashSet<&String> = path.iter().collect();
    append.into_iter().fold(Vec::new(), |mut acc, p| {
        if !pth.contains(&p) && !acc.contains(&p) {
            acc.push(p);
        }
        acc
    })
}

// split unix PATH to paths via ':' separator
fn split_path(path: &str) -> Vec<String> {
    path.split(':').map(|s| s.to_owned()).collect()
}

fn unique(paths: Vec<String>) -> Vec<String> {
    let mut hset = HashSet::with_capacity(paths.len());
    paths
        .into_iter()
        .flat_map(|pth| {
            if hset.insert(pth.clone()) {
                Some(pth)
            } else {
                None
            }
        })
        .collect()
}
