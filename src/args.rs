use clap::Parser;

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
pub(crate) struct CmdArgs {
    /// List of paths to append
    #[arg(short, long)]
    pub(crate) append: Vec<String>,
    /// List of paths to prepend
    #[arg(short, long)]
    pub(crate) prepend: Vec<String>,
    /// Path env variable provided as argument
    #[arg(long)]
    pub(crate) path: Option<String>,
    /// Remove duplicates
    #[arg(short, long, default_value = "true")]
    pub(crate) unique: bool,
}
