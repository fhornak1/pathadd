# pathadd

**Utility for easy alternation of environment PATH variable.**

This utility can append or prepend paths for executables not already present in the env `$PATH` variable.
I created it to simplify path modifications in `.zporfile` and `.zshrc` files without worrying about duplicated entries.

## Usage

You can specify your path argument. This is useful when you don't have exported PATH yet.
```bash
## Content of .bashrc or .zshrc or something similiar
export PATH=`pathadd -p "$HOME/.cargo/bin" -p $HOME/.local/bin --path $PATH`
```

Or if you do not specify the `--path` option, it will be loaded from env variables.

```bash
export PATH=`pathadd -p $HOME/.cargo/bin -p $HOME/.local/bin
```

